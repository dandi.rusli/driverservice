FROM scratch
COPY ./driverservice /go/bin/driverservice
EXPOSE 18000
ENTRYPOINT ["/go/bin/driverservice"]
