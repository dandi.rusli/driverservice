package app

import (
	"errors"
	"github.com/json-iterator/go"
	"net/http"
	"strconv"
)

var json = jsoniter.ConfigCompatibleWithStandardLibrary

const (

	// range driver id
	IDRangeLow   int = 1
	IDRangeHigh  int = 50000

	// default values
	DriverRadius     = 500
	DriverLimit      = 10
)

func getFloat64Param(r *http.Request, param string, isMandatory bool, defaultValue float64) (float64, error) {
	val := r.URL.Query().Get(param)
	if isMandatory && val == "" {
		return 0, errors.New("Parameter not found: " + param)
	}

	if val == "" {
		return defaultValue, nil
	}

	res, err := strconv.ParseFloat(val, 64)
	if err != nil {
		return 0, errors.New("Non float value: " + param)
	}

	return res, nil
}

func getIntParam(r *http.Request, param string, isMandatory bool, defaultValue int) (int, error) {
	val := r.URL.Query().Get(param)
	if isMandatory && val == "" {
		return 0, errors.New("Parameter not found: " + param)
	}

	if val == "" {
		return defaultValue, nil
	}

	res, err := strconv.Atoi(val)
	if err != nil {
		return 0, errors.New("Non int value: " + param)
	}

	return res, nil
}

func respondWithError(w http.ResponseWriter, rc int, message [] string) {
	if len(message) > 0 {
		respondWithJSON(w, rc, map[string][] string{"errors": message})
	} else {
		respondWithJSON(w, rc, map[string][] string{})
	}
}

func respondWithJSON(w http.ResponseWriter, rc int, body interface{}) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(rc)
	response, _ := json.Marshal(body)
	w.Write(response)
}
