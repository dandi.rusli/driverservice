package app

import (
	"driverservice/app/db"
	"driverservice/app/model"
	"log"
	"net/http"
)

// GetDrivers http handler (GET)
func (s *Service) GetDrivers(w http.ResponseWriter, r *http.Request) {

	// param validations
	lat, lon, radius, limit, errMsg := parseGetDriversParam(r)
	if len(errMsg) > 0 {
		respondWithError(w, http.StatusUnprocessableEntity, errMsg)
		return
	}

	// get nearest driver on db
	nearestDrivers, err := db.GetNearestDriver(s.Session, lat, lon, radius, limit)
	if err != nil {
		log.Println("Unexpected db err ", err)
		respondWithError(w, http.StatusInternalServerError, []string{})
	}

	// map to response
	res := make([]model.HTTPGetDriverResponse, 0)
	for _, driver := range nearestDrivers {
		res = append(res, model.HTTPGetDriverResponse{
			ID:        driver.Doc.ID,
			Latitude:  driver.Doc.Location.Lat,
			Longitude: driver.Doc.Location.Lon,
			Distance:  driver.Dist,
		})
	}

	respondWithJSON(w, http.StatusOK, res)
}

func parseGetDriversParam(r *http.Request) (float64, float64, float64, int, []string) {

	var errMsg []string
	lat, err := getFloat64Param(r, "latitude", true, 0)
	if err != nil {
		errMsg = append(errMsg, err.Error())
	}

	lon, err := getFloat64Param(r, "longitude", true, 0)
	if err != nil {
		errMsg = append(errMsg, err.Error())
	}

	radius, err := getFloat64Param(r, "radius", false, DriverRadius)
	if err != nil {
		errMsg = append(errMsg, err.Error())
	}

	limit, err := getIntParam(r, "limit", false, DriverLimit)
	if err != nil {
		errMsg = append(errMsg, err.Error())
	}

	if lat < float64(-90) || lat > float64(90) {
		errMsg = append(errMsg, "Latitude should be between +/- 90")
	}

	if lon < float64(-180) || lon > float64(180) {
		errMsg = append(errMsg, "Longitude should be between +/- 180")
	}

	return lat, lon, radius, limit, errMsg
}
