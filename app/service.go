package app

import (
	"driverservice/app/db"
	"github.com/gorilla/mux"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
	"log"
	"net/http"
)

type Service struct {
	Router  *mux.Router
	Session *r.Session
}

func (s *Service) Init(dbAddress, dbName string, dbPoolInit, dbPoolMax int) {

	// http handlers
	s.Router = mux.NewRouter()
	s.Router.HandleFunc("/drivers", s.GetDrivers).Methods("GET")
	s.Router.HandleFunc("/drivers/{id}/location", s.PutDriverLocation).Methods("PUT")

	// database
	log.Println("Connecting to Database")
	s.Session = db.Init(dbAddress, dbName, dbPoolInit, dbPoolMax)
}

func (s *Service) Run(port string) {
	log.Println("Starting HTTP Listener")
	log.Fatal(http.ListenAndServe(port, s.Router))
}
