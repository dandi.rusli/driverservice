package model

import "gopkg.in/rethinkdb/rethinkdb-go.v5/types"

type DbDriver struct {
	ID        string      `rethinkdb:"id,omitempty"`
	Location  types.Point `rethinkdb:"location,omitempty"`
	Accuracy  float64     `rethinkdb:"accuracy,omitempty"`
	Timestamp string      `rethinkdb:"timestamp,omitempty"`
}

type NearestDriver struct {
	Dist int32    `rethinkdb:"dist,omitempty"`
	Doc  DbDriver `rethinkdb:"doc,omitempty"`
}
