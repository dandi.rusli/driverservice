package model

type HTTPGetDriverResponse struct {
	ID        string  `json:"id,omitempty"`
	Latitude  float64 `json:"latitude,omitempty"`
	Longitude float64 `json:"longitude,omitempty"`
	Distance  int32   `json:"distance"`
}

type HTTPPutDriverRequest struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
	Accuracy  float64 `json:"accuracy"`
}
