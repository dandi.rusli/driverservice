package db

import (
	"driverservice/app/model"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
	"gopkg.in/rethinkdb/rethinkdb-go.v5/types"
	"log"
	"strings"
	"time"
)

var (
	tableName = "location"
	rTable    = r.Table(tableName)
)

func Init(dbAddress, dbName string, poolInit, poolMax int) (*r.Session) {

	// create db if not exist
	err := createDb(dbAddress, dbName)
	if err != nil {
		log.Fatal("Could not init database: ", err)
	}

	// connect to db
	dbAddressArr := strings.Split(dbAddress, ",")
	session, err := r.Connect(r.ConnectOpts{
		Addresses: dbAddressArr,
		Database:   dbName,
		InitialCap: poolInit,
		MaxOpen:    poolMax,
	})

	if err != nil {
		log.Fatal("Could not connect to database: ", err)
	}

	return session
}

func PutDriverLocation(session *r.Session, id string, payload model.HTTPPutDriverRequest) {

	var data = map[string]interface{}{
		"id":        id,
		"location":  types.Point{Lat: payload.Latitude, Lon: payload.Longitude},
		"accuracy":  payload.Accuracy,
		"timestamp": time.Now().Unix(),
	}

	// try update first since it'll happen more than insert
	result, _ := rTable.Get(id).Update(data, r.UpdateOpts{Durability:"soft"}).RunWrite(session)
	if result.Updated == 0 {
		rTable.Insert(data, r.InsertOpts{ReturnChanges: false, Durability: "soft"}).RunWrite(session)
	}

}

func GetNearestDriver(session *r.Session, lat, lon, radius float64, limit int) ([]model.NearestDriver, error) {

	rows, err := rTable.GetNearest(r.Point(lon, lat), r.GetNearestOpts{
		Index:      "location",
		MaxDist:    radius,
		MaxResults: limit,
		Unit:       "m",
	}).Run(session)

	if err != nil {
		return nil, err
	}

	var nearest []model.NearestDriver
	err = rows.All(&nearest)
	if err != nil {
		return nil, err
	}

	return nearest, nil
}

func createDb(dbaddress, dbname string) (error) {
	dbAddressArr := strings.Split(dbaddress, ",")
	session, err := r.Connect(r.ConnectOpts{Address: dbAddressArr[0]})
	if err != nil {
		return err
	}

	// if db and/or table exists, this will be ignored
	r.DBCreate(dbname).RunWrite(session)
	r.DB(dbname).TableCreate(tableName, r.TableCreateOpts{Durability: "soft"}).RunWrite(session)
	r.DB(dbname).Table(tableName).IndexCreate("location", r.IndexCreateOpts{Geo: true}).RunWrite(session)
	r.DB(dbname).Table(tableName).IndexWait("location").RunWrite(session)

	session.Close()
	return nil
}
