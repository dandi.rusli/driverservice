package app

import (
	"driverservice/app/db"
	"driverservice/app/model"
	"errors"
	"github.com/gorilla/mux"
	"net/http"
	"strconv"
)

// PutDriverLocation http handler (PUT)
func (s *Service) PutDriverLocation(w http.ResponseWriter, r *http.Request) {

	// get driver id and validate
	id := mux.Vars(r)["id"]
	err := validateDriverID(id)
	if err != nil {
		respondWithError(w, http.StatusNotFound, []string{})
		return
	}

	// get payload and validate
	var payload model.HTTPPutDriverRequest
	json.NewDecoder(r.Body).Decode(&payload)

	errMsg, err := validatePutDriverLocationRequest(payload)
	if err != nil {
		respondWithError(w, http.StatusUnprocessableEntity, errMsg)
		return
	}

	// put to db
	go db.PutDriverLocation(s.Session, id, payload)
}

func validatePutDriverLocationRequest(payload model.HTTPPutDriverRequest) ([]string, error) {
	var errMsg []string

	// zero lat lon is impossible
	if payload.Latitude == 0 {
		errMsg = append(errMsg, "Mandatory field: Latitude")
	}

	if payload.Longitude == 0 {
		errMsg = append(errMsg, "Mandatory field: Longitude")
	}

	if payload.Latitude < float64(-90) || payload.Latitude > float64(90) {
		errMsg = append(errMsg, "Latitude should be between +/- 90")
	}

	if payload.Longitude < float64(-180) || payload.Longitude > float64(180) {
		errMsg = append(errMsg, "Longitude should be between +/- 180")
	}

	if payload.Accuracy > float64(200) {
		errMsg = append(errMsg, "Max accepted accuracy is 200m")
	}

	if len(errMsg) > 0 {
		return errMsg, errors.New("bad payload")
	}

	return []string{}, nil
}

func validateDriverID(input string) (error) {

	id, err := strconv.Atoi(input)
	if err != nil {
		return errors.New("non numeric id")
	}

	if int(id) < IDRangeLow || id > IDRangeHigh {
		return errors.New("invalid range id")
	}

	return nil
}
