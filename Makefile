include .env
APP=driverservice
DB=rethinkdb

all: dependencies test build run

dependencies:
	go get -d -v

test: dependencies
	go test -v

build: test
	go build -v -o $(APP)
	CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o ./driverservice
	sudo docker build -t $(APP) .

run: build
	sudo docker run -it --rm -p $(SERVER_PORT)$(SERVER_PORT) --link $(DB) \
        -e SERVER_PORT=$(SERVER_PORT) \
        -e DB_ADDRESS=$(DB):28015 \
        -e DB_NAME=driver \
        -e DB_POOL_INIT=10 \
        -e DB_POOL_MAX=50 \
        $(APP)

clean:
	go clean




