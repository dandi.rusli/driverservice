# How to :
#   1) $pip install locustio
#   2) $locust -f getdriver_locustfile.py --host=http://localhost:8000 -P 8079
#   3) Open browser at http://localhost:8079/
#      Run with at least 20 user, with 10 hatch time
#
#   Headless commandline:
#   > locust -f getdriver_locustfile.py --host=http://localhost:8000  --no-web -c 50 -r 10 --csv putdriver -t 600s
#

from locust import HttpLocust, TaskSet, task
import json, random

# DKI Jakarta approx lat lon range
X_LOW  = 106.700000
X_HIGH = 106.999999
Y_LOW  =  -6.120000
Y_HIGH =  -6.299999

# Tasks
class GetNearestDriver(TaskSet):

    def on_start(self):
        print ("on_start")

    @task(1)
    def getdriver(self):

        # random lat, lon
        rlat = round(random.uniform(Y_LOW, Y_HIGH), 6)
        rlon = round(random.uniform(X_LOW, X_HIGH), 6)

        endpoint = '/drivers' + '?latitude=' + str(rlat) + '&longitude=' + str(rlon) + '&radius=500&limit=10'
        #print ("GET " + endpoint)

        self.client.get(endpoint)


# Main
class GetDrivers(HttpLocust):
    task_set = GetNearestDriver
    min_wait = 1000
    max_wait = 1000
