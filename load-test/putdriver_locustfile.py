# How to :
#   1) $pip install locustio
#   2) $locust -f putdriver_locustfile.py --host=http://localhost:8000 -P 8089
#   3) Open browser at http://localhost:8089/
#      Run with at least 50 user, with 10 hatch time
#
#   Headless commandline:
#   > locust -f putdriver_locustfile.py --host=http://localhost:8000  --no-web -c 50 -r 10 --csv putdriver -t 600s
#

from locust import HttpLocust, TaskSet, task
import json, random

# seed data holder
USER_DATA = []

# DKI Jakarta approx lat lon range
X_LOW  = 106.700000
X_HIGH = 106.999999
Y_LOW  =  -6.120000
Y_HIGH =  -6.299999

# Tasks
class UpdateLocation(TaskSet):

    id = "0"
    lat = 1.0
    lat = 1.0
    acc = 1.0

    def on_start(self):
        print ("on_start")

    @task(1)
    def update(self):

        # seed array
        if len(USER_DATA) < 1:

            print ("Seeding user data... ")
            for i in range(50000, 0, -1):

                # random lat, lon, accuracy
                rlat = round(random.uniform(Y_LOW, Y_HIGH), 6)
                rlon = round(random.uniform(X_LOW, X_HIGH), 6)
                racc = round(random.uniform(0.1, 29.9), 1)

                item = (i, rlat, rlon,racc)
                USER_DATA.append(item)

        #
        self.id, self.lat, self.lon, self.acc = USER_DATA.pop()
        endpoint = "/drivers/" + str(self.id) + "/location"

        #print ("PUT " + endpoint)
        #print (str(self.lat) + " " + str(self.lon))

        #
        headers = {'content-type': 'application/json'}
        payload = {
                "latitude": self.lat,
                "longitude": self.lon,
                "accuracy": self.acc
        }

        self.client.put(endpoint, data=json.dumps(payload), headers=headers)


# Main
class Drivers(HttpLocust):
    task_set = UpdateLocation
    min_wait = 100
    max_wait = 100
