package main_test

import (
	"bytes"
	"driverservice/app"
	"driverservice/app/model"
	"encoding/json"
	"fmt"
	r "gopkg.in/rethinkdb/rethinkdb-go.v5"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
	"time"
)

var service app.Service

func TestMain(m *testing.M) {

	dbAddress := "localhost:28015"
	dbName := "drivertest"

	// drop database
	err := dropDb(dbAddress, dbName)
	if err != nil {
		fmt.Print("Failure init test ", err)
	}

	// init app
	service = app.Service{}
	service.Init(dbAddress, dbName, 1, 1)
	code := m.Run()
	os.Exit(code)
}

func dropDb(dbaddress, dbname string) (error) {
	session, err := r.Connect(r.ConnectOpts{Address: dbaddress})
	if err != nil {
		return err
	}

	r.DBDrop(dbname).RunWrite(session)
	session.Close()
	return nil
}

//
func TestPutDriverLocation200(t *testing.T) {

	// populate 5 points
	locations := []struct {
		id  string
		lat float64
		lon float64
	}{
		{"101", -6.2801041, 106.8165594}, // naya kemang
		{"102", -6.2800485, 106.8180752}, // sederhana ampera
		{"103", -6.2814117, 106.8143052}, // pejaten village
		{"104", -6.2807712, 106.8199987}, // unas
		{"105", -6.2804511, 106.8381611}, // halim
	}

	for _, loc := range locations {
		body := location(loc.lat, loc.lon, 1)
		req, _ := http.NewRequest("PUT", "/drivers/"+loc.id+"/location", bytes.NewBuffer(body))
		res := executeRequest(req)
		checkResponseCode(t, http.StatusOK, res.Code)
	}

	// give db some time to flush
	time.Sleep(3 * time.Second)

}

func TestPutDriverLocation4XX(t *testing.T) {

	body := location(-6.2801041, 106.8165594, 1)

	// bad url
	{
		req, _ := http.NewRequest("PUT", "/drivers/1/locationX", bytes.NewBuffer(body))
		res := executeRequest(req)
		checkResponseCode(t, http.StatusNotFound, res.Code)
	}

	// non numeric id
	{
		req, _ := http.NewRequest("PUT", "/drivers/x/location", bytes.NewBuffer(body))
		res := executeRequest(req)
		checkResponseCode(t, http.StatusNotFound, res.Code)
	}

	// id above range
	{
		req, _ := http.NewRequest("PUT", "/drivers/50001/location", bytes.NewBuffer(body))
		res := executeRequest(req)
		checkResponseCode(t, http.StatusNotFound, res.Code)
	}

	{
		// input above ranges
		body := location(-600.2801041, 1060.8165594, 1000)
		req, _ := http.NewRequest("PUT", "/drivers/5000/location", bytes.NewBuffer(body))
		res := executeRequest(req)
		checkResponseCode(t, http.StatusUnprocessableEntity, res.Code)

		var m map[string][]string
		json.NewDecoder(res.Body).Decode(&m)
		if len(m["errors"]) != 3 {
			t.Errorf("Expected 3 error message. Got %d\n", len(m["errors"]))
		}
	}

	{
		// missing lat
		body = []byte(`{
			"longitude": 77.59463452,
			"accuracy": 10
		}`)
		req, _ := http.NewRequest("PUT", "/drivers/5000/location", bytes.NewBuffer(body))
		res := executeRequest(req)
		checkResponseCode(t, http.StatusUnprocessableEntity, res.Code)
		checkErrMsg(t, res, "Mandatory field: Latitude")
	}

	{
		// missing lon
		body = []byte(`{
			"latitude": 12.97161923,
			"accuracy": 10
		}`)
		req, _ := http.NewRequest("PUT", "/drivers/5000/location", bytes.NewBuffer(body))
		res := executeRequest(req)
		checkResponseCode(t, http.StatusUnprocessableEntity, res.Code)
		checkErrMsg(t, res, "Mandatory field: Longitude")
	}
}

//
// Get Driver
func TestGetDrivers200(t *testing.T) {

	// 200m from naya kemang
	url := "/drivers?latitude=-6.2801041&longitude=106.8165594&radius=200&limit=10"

	req, _ := http.NewRequest("GET", url, nil)
	res := executeRequest(req)
	checkResponseCode(t, http.StatusOK, res.Code)

	var resArr []model.HTTPGetDriverResponse
	json.Unmarshal(res.Body.Bytes(), &resArr)

	if len(resArr) != 2 {
		t.Errorf("Expected for 2 drivers. Got '%d'", len(resArr))
	}

	found := false
	for _, driver := range resArr {
		if driver.ID == "101" || driver.ID == "102" {
			found = true
		}
	}

	if !found {
		t.Errorf("Expecting for 101 or 102 drivers ID. Received '%v'", resArr)
	}

}

func TestGetDrivers4XX(t *testing.T) {

	url := "/drivers"
	req, _ := http.NewRequest("GET", url, nil)
	res := executeRequest(req)
	checkResponseCode(t, http.StatusUnprocessableEntity, res.Code)
}

//
// share func
func location(lat, lon, acc float64) ([]byte) {

	loc := model.HTTPPutDriverRequest{
		Latitude:  lat,
		Longitude: lon,
		Accuracy:  acc,
	}

	bb := new(bytes.Buffer)
	json.NewEncoder(bb).Encode(loc)
	return bb.Bytes()
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	service.Router.ServeHTTP(rr, req)
	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected HTTP RC %d. Got %d\n", expected, actual)
	}
}

func checkErrMsg(t *testing.T, res *httptest.ResponseRecorder, expected string) {
	var m map[string][]string
	json.NewDecoder(res.Body).Decode(&m)
	actual := m["errors"][0]
	if expected != actual {
		t.Errorf("Expected Err %s. Got [%s]\n", expected, actual)
	}
}
