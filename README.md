# DriverService

This is a simple REST API service that simulate client request to get the nearest driver from their location, as part of evaluation for open roles at Go-Jek Engineering.

## Requirement
The requirements is to provide REST API consists of two services
   * PUT `/drivers/{id}/location`, with expected load of 50000 request/minute (~833 request/s)
   * GET `/drivers`, with expected load of 20 users concurency

On top of this I'm adding my own requirements
*  It has to be build on tech that I'm not familiar with so I'm learning something new from this experience, thus ditching my usual stack of Java/Python + RDMS. 
*  It has to be as easy as possible to setup and test, thus eliminating Cloud solutions


### Solution Overview
I decided to **Golang** and **RethinkDB** for this solution.

* *Golang*: because it's proven to be robust and as a language (as I learned later on) is fun to write. 
* *RethinkDB*: As alternative to other NoSQL platform such as MongoDB or Cassandra. The documentation is solid so I thought I could give it a try
* *Locust.io*: for load test tool. Because it's simple enough to use


## Setup Instructions
In order to run, you need
- Docker 18.x
- RethinkDB running as docker container
- Python 2.7 or 3.x and pip installed, for load testing


####
**Step (1) Run RethinkDB as Container**
```shell
sudo docker run -d --name rethinkdb -p 28015:28015 rethinkdb
``` 

**Step (2) Make and Run the application**
```shell
$ make
```

Sample output
```shell
$ make
go get -d -v
go test -v
2018/11/05 07:50:57 Connecting to Database
=== RUN   TestPutDriverLocation200
--- PASS: TestPutDriverLocation200 (3.01s)
=== RUN   TestPutDriverLocation4XX
--- PASS: TestPutDriverLocation4XX (0.00s)
=== RUN   TestGetDrivers200
--- PASS: TestGetDrivers200 (0.02s)
=== RUN   TestGetDrivers4XX
--- PASS: TestGetDrivers4XX (0.00s)
PASS
ok  	driverservice	3.406s
go build -v -o driverservice
CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -installsuffix cgo -ldflags="-w -s" -o ./driverservice
sudo docker build -t driverservice .
Sending build context to Docker daemon  10.79MB
Step 1/4 : FROM scratch
 --->
Step 2/4 : COPY ./driverservice /go/bin/driverservice
 ---> Using cache
 ---> 7ca6498be231
Step 3/4 : EXPOSE 18000
 ---> Using cache
 ---> 32f8fe3a348c
Step 4/4 : ENTRYPOINT ["/go/bin/driverservice"]
 ---> Using cache
 ---> 6d99392628ff
Successfully built 6d99392628ff
Successfully tagged driverservice:latest
sudo docker run -it --rm -p :18000:18000 --link rethinkdb \
        -e SERVER_PORT=:18000 \
        -e DB_ADDRESS=rethinkdb:28015 \
        -e DB_NAME=driver \
        -e DB_POOL_INIT=10 \
        -e DB_POOL_MAX=50 \
        driverservice
2018/11/05 00:51:09 Connecting to Database
2018/11/05 00:51:09 Starting HTTP Listener
```

To keep things simple, the container is running on foreground. 
Please keep it running until we finished the load test below.

## Load Test
###
Pre-req: you need to install locust.io 
```shell 
pip install locustio
```

Run the load test
```shell
./run-load-test.sh
```

The script will simulate PutDriver and GetDriver in sequence, each run for 2 minutes.
You can view the result in generated CSV files.
- putdriver_requests.csv
- getdriver_requests.csv
 


## Performance Result

### 1. **PUT Driver Location** Result

| Concurrency   | Median (ms)   | Max (ms)  | RPS
| ------------: | ------------: | --------: | --:
| 1             |  4            | 5         |   9
| 10            |  6            | 14        |  88
| 25            | 14            | 30        | 195
| 50            | 33            | 69        | 331
| 75            | 63            | 100       | 404
| 100           | 97            | 220       | 418
| 150           | 180           | 540       | 362
| 200           | 270           | 760       | 359 

### 2. **GET Nearest Driver** Result

| Concurrency   | Median (ms)   | Max (ms)  | RPS
| ------------: | ------------: | --------: | --:
| 1             | 24            | 29        |   1
| 5             | 49            | 70        |   4
| 10            | 61            | 81        |   9
| 20            | 83            | 180       |  17


* Suprisingly, not as good as I thought it would be. Further analysist shows that bottleneck in RethinkDB performance and the App is not maximizing CPU enough. 
* In order to pass the requirement of 833 RPS, I'm afraid single instance won't be able to do so. It has to be in *multi instances* setup, for both the App and Database.


## Afterthought
It was fun experiment, though I failed to reach 833 RPS on single instance. Further tuning to the application code and database settings need to be research to reach that point. Or maybe I have to *rethink* of using RethinkDB. But could be just because I haven't research it enough. Anyway, it was fun! Cheers.