package main

import (
	"driverservice/app"
	"os"
	"strconv"
)

func main() {

	// get env variable
	dbAddress := getEnv("DB_ADDRESS", "localhost:28015")
	dbName := getEnv("DB_NAME", "driver")
	dbPoolInit, _ := strconv.Atoi(getEnv("DB_POOL_INIT", "25"))
	dbPoolMax, _ := strconv.Atoi(getEnv("DB_POOL_MAX", "512"))
	serverPort := getEnv("SERVER_PORT", ":8000")

	// start service
	service := app.Service{}
	service.Init(dbAddress, dbName, dbPoolInit, dbPoolMax)
	service.Run(serverPort)

}

func getEnv(key, defaultValue string) (string) {
	res := os.Getenv(key)
	if res == "" {
		return defaultValue
	}
	return res
}
